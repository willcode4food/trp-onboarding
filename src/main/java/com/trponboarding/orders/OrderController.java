package com.trponboarding.orders;
import lombok.Data;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Data
@Slf4j
@RestController
public class OrderController {
	private List<Order> allOrders;

	OrderController(){
		this.allOrders = new ArrayList<Order>();
	}

	@GetMapping("/orders")
	List<Order> showOrders(){
		return this.allOrders;
	}
	@PostMapping("/orders")
	String newOrder(@RequestBody Order newOrder){
		Order sentOrder = new Order();
		this.allOrders.add(sentOrder);
		log.info("New Order ! " + sentOrder.getId());
		return sentOrder.getId();
	}
}
